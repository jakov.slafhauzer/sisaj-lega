from django.urls import include, path
from django.contrib import admin
from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.pocetna, name='pocetna'),
    path('kratke/', views.kratke, name='kratke'),
    path('duge/', views.duge, name='duge'),
    path('fades/', views.fades, name='fades'),
    path('alati/', views.alati, name='alati'),
    path('kontakt/', views.kontakt, name='kontakt'),
    path('detector/', include('detector.urls')),
    path('accounts/', include('accounts.urls')),
]
