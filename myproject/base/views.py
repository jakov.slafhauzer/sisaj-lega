from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.urls import reverse


def pocetna(request):
    context = []
    return render(request, 'base/pocetna.html')

def kratke(request):
    context = []
    return render(request, 'base/kratke.html')

def duge(request):
    context = []
    return render(request, 'base/duge.html')

def fades(request):
    context = []
    return render(request, 'base/fades.html')

def alati(request):
    context = []
    return render(request, 'base/alati.html')

def kontakt(request):
    context = []
    return render(request, 'base/kontakt.html')