from __future__ import print_function
import cv2 as cv
import argparse
import numpy as np
from detector.models import PredictShape
#import detector.train_model as dtc
import re

class Main:
    """
    Main class handles all operations of facial detection
    and face shape calcaultions
    """

    def __init__(self,_image) -> None:
        self._LBFModel = "detector/data/lbfmodel.yaml"   
        self._haarcascade = "detector/data/lbpcascade_frontalface.xml"  
        self._parser = argparse.ArgumentParser(
            description="Code for facial recognition")
        self._args = None
        self._face_cascade = None
        self._landmark_detector = None
        self._image = _image

    
    def run_detection_stillshot(self):
        """
        Begin face detecion on single image
        """

        self.create_face_cascade()
        self.create_LM_detector()

        if self._image is None:
            return False

        image = cv.imread(self._image)
        target_width = 800
        ratio = target_width / image.shape[1]
        image = cv.resize(image, (target_width, int(image.shape[0] * ratio)))

        result = self.detect_and_display(image, self._landmark_detector, "stillshot")

        cv.waitKey(0)
        return result
    

    def detect_and_display(self, frame, landmark_detector, method):
        """
        Dection of face and its landmarks, visual plots
        """

        frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        frame_gray = cv.equalizeHist(frame_gray)

        faces = self._face_cascade.detectMultiScale(
            frame_gray, scaleFactor=1.2, minNeighbors=5)

        landmark_points = []

        for (x, y, w, h) in faces:
            forehead = y
            forhead_mid = (x + (x+w)) // 2

            _, landmarks = landmark_detector.fit(frame_gray, faces)
            for landmark in landmarks:
                for x, y in landmark[0]:
                    landmark_points.append((int(x), int(y)))

            if len(landmark_points) >= 68:
                cheek_left = landmark_points[1]
                cheek_right = landmark_points[15]
                chin_left = landmark_points[6]
                chin_right = landmark_points[10]
                nose_left = landmark_points[3]
                nose_right = landmark_points[13]
                eye_brow_left = landmark_points[17]
                eye_brow_right = landmark_points[26]
                bottom_chin = landmark_points[8]

                cheek_bone_right_down_one = landmark_points[11]

                cheek_distance = cheek_right[0] - cheek_left[0]
                top_jaw_distance = nose_right[0] - nose_left[0]
                forehead_distance = eye_brow_right[0] - eye_brow_left[0]
                chin_distance = chin_right[0] - chin_left[0]
                head_lenghth = bottom_chin[1] - forehead
                
                jaw_width = top_jaw_distance
                jaw_right_to_down_one = cheek_bone_right_down_one[1] - \
                    nose_right[1]

                jaw_left_to_down_one = cheek_bone_right_down_one[0] - \
                    nose_left[0]

                jaw_angle = self._calculate_angle(
                    jaw_width, jaw_right_to_down_one, jaw_left_to_down_one)

                result = self.calculate_face_shape(
                    cheek_distance, top_jaw_distance, forehead_distance, chin_distance, head_lenghth, frame, jaw_angle, method)

        return result

    def calculate_face_shape(self, cheek, jaw, forehead, chin, head_length, frame, jaw_angle, method):
        """
        Takes 4 parameters
        cheek, jaw, forehead, chin - distances
        calculates facial shape 
        """
        cheek_ratio = cheek / head_length
        jaw_ratio = jaw / head_length
        forehead_ratio = forehead / head_length
        chin_ratio = chin / head_length
        head_ratio = head_length / cheek

        result = "Loading..."

        hairstyles = {
            "okrugli oblik.": ["crew cut", "ivy League", "srednji prijelaz s većom dužinom na vrhu"],
            "ovalni oblik.": ["undercut", "taper fade", "french crop", "mullet"],
            "kvadratni oblik.": ["buzz cut", "frizure s prijelazom", "undercut"],
            "izduženi oblik (pravokutnik).": ["ivy League", "french crop", "kraća valovita", "samurai rep"],
            "oblik srca.": ["boho valovi", "duža valovita kosa", "frizure s prijelazom", "taper fade"],
            "dijamantni oblik.": ["duža razdijeljena", "burst fade", "undercut", "crew cut"]
        }

        # Round Face
        if (
            0.8 <= cheek_ratio <= 1.0 and
            0.7 <= jaw_ratio <= 0.8 and
            0.6 <= forehead_ratio <= 0.8 and
            0.3 <= chin_ratio <= 0.4 and
            head_ratio <= 1.25 and jaw_angle <= 50.0
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - okrugli oblik."

        # Oval Face
        elif (
            0.5 <= cheek_ratio <= 0.8 and
            0.5 <= jaw_ratio <= 0.7 and
            0.5 <= forehead_ratio <= 0.7 and
            0.2 <= chin_ratio <= 0.4 and
            1.25 <= head_ratio <= 1.6 and jaw_angle > 50.0
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - ovalni oblik."

        # Rectangle Face
        elif (
            0.5 <= cheek_ratio <= 0.8 and
            0.5 <= jaw_ratio <= 0.8 and
            0.5 <= forehead_ratio <= 0.8 and
            0.3 <= chin_ratio <= 0.4 and
            head_ratio >= 1.30 and jaw_angle > 55
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - izduženi oblik (pravokutnik)."

        # Square Face
        elif (
            0.7 <= cheek_ratio <= 0.99 and
            0.7 <= jaw_ratio <= 0.8 and
            0.6 <= forehead_ratio <= 0.99 and
            0.3 <= chin_ratio <= 0.5 and
            head_ratio <= 1.29 and jaw_angle < 55
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - kvadratni oblik."

        # Heart-Shaped Face
        elif (
            0.7 <= cheek_ratio <= 0.8 and
            0.7 <= jaw_ratio <= 0.8 and
            0.5 <= forehead_ratio <= 0.7 and
            0.3 <= chin_ratio <= 0.4 and
            1.2 <= head_ratio <= 1.4
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - oblik srca."

        # Diamond Shaped Face
        elif (
            0.7 <= cheek_ratio <= 0.8 and
            0.7 <= jaw_ratio <= 0.8 and
            0.5 <= forehead_ratio <= 0.7 and
            0.2 <= chin_ratio <= 0.4 and
            1.2 <= head_ratio <= 1.4 and jaw_angle >= 55.0
        ):
            result = "Tvoj oblik lica postignut računanjem karakteristika lica je - dijamantni oblik."

        # If none of the conditions match
        else:
            result = "Nije prepoznat oblik lica računanjem karakteristika lica."

        hairstyle_suggestions = []
        if result:
            # Correct shape_key extraction to match the dictionary key format
            shape_key = result.replace("Tvoj oblik lica postignut računanjem karakteristika lica je - ", "").strip()
            
            # Fetch hairstyles based on face shape
            hairstyle_suggestions = hairstyles.get(shape_key, [])

        if method == "stillshot":
            decision_tree = PredictShape([cheek_ratio, jaw_ratio, forehead_ratio, chin_ratio, head_ratio, jaw_angle])
            classification = decision_tree.train_model()
            classification_result = classification[1][1][0]

            cv.putText(frame, str("Classification: " + classification_result), (10, 70),
                    cv.FONT_HERSHEY_DUPLEX, .5, (0, 0, 0), 1)

        cv.putText(frame, str(result), (10, 50),
                cv.FONT_HERSHEY_DUPLEX, .5, (0, 0, 0), 1)
        

        if shape_key == "okrugli oblik.":
            opis_oblika = " Okrugli oblik lica podrazumijeva mekane i zaobljene crte lica. Dužina i širina su uglavnom jednake veličine, dok su jagodice najširi dio lica, davajući licu zaobljeni oblik bez oštrih kutova."
            result += opis_oblika
        
        elif shape_key == "ovalni oblik.":
            opis_oblika = " Ovalni oblik sadrži uravnotežene proporcije lica s malo širim čelom od zaobljene brade. Ovo je lice zbog blago suženog oblika i jednakih kontura iznimno prilagodljivo raznim vrstama frizura."
            result += opis_oblika
       
        elif shape_key == "izduženi oblik (pravokutnik).":
            opis_oblika = " Kao što mu i naziv govori, ovo lice je više izduženo nego što je široko. Ima četvrtastu čeljust i jednako široko čelo te kombinira uglate značajke četvrtastog lica s duljinom ovalnog lica, stvarajući upečatljiv, izdužen izgled."
            result += opis_oblika
        
        elif shape_key == "kvadratni oblik.":
            opis_oblika = " Kvadratno lice karakterizira snažna crta čeljusti te čelo i čeljust otprilike jednake širine. Cjelokupni izgled uravnotežen je oštrim rubovima, pružajući hrabar i definiran izgled."
            result += opis_oblika
        
        elif shape_key == "oblik srca.":
            opis_oblika = " Lice u obliku srca ima široko čelo koje se sužava prema uskoj, šiljatoj bradi. Jagodice su tipično izražene, dajući licu upečatljiv izgled obrnutog trokuta."
            result += opis_oblika

        elif shape_key == "dijamantni oblik.":
            opis_oblika = " Dijamantni oblik lica ima visoke, istaknute jagodice s uskim čelom i linijom čeljusti. Lice se sužava prema šiljatoj bradi, stvarajući dramatičan, uglati izgled."
            result += opis_oblika
        
        else:   
            result


        if result:
            hairstyle_str = ", ".join(hairstyle_suggestions)
            final_result = f"Rezultat detekcije treniranog modela je: {classification_result}\n{result}\nPredložene frizure za tvoj oblik lica: {hairstyle_str}."
            return final_result
        else:
            final_result = f"Rezultat detekcije treniranog modela je: {classification_result}\nNema predloženih frizura za tvoj oblik lica."
            return final_result

    

    def _calculate_angle(self, c, b, a):
        """
        Caculates the angle of the jaw using law of cosines 
        """
        cosine_angle = (b**2 + c**2 - a**2) / (2 * b * c)

        jaw_angle_degrees = np.degrees(np.arccos(cosine_angle))

        return jaw_angle_degrees



    def create_LM_detector(self):
        """
        Creates landmark detector 
        loading lbf model
        """
        self._landmark_detector = cv.face.createFacemarkLBF()
        self._landmark_detector.loadModel(self._LBFModel)

    def create_face_cascade(self):
        """
        Creates face cascade
        """
        self._face_cascade = cv.CascadeClassifier(self._haarcascade)
