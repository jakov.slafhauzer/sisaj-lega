from django.db import models
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
import detector.data.modelData as data

class PredictShape:
    def __init__(self, featureVector) -> None:
        self._new_feature = featureVector

    def train_model(self):
        """
        Training a DTC for prediction classification 
        """
        X  = data.X
        y = data.y
        
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=41)

        model = DecisionTreeClassifier()

        model.fit(X_train, y_train)

        new_feature_vector = self._new_feature

        predictions = model.predict([new_feature_vector])
        y_test_predictions = model.predict(X_test)

        accuracy = accuracy_score(y_test, y_test_predictions)

        return [["accuracy", accuracy * 100], [ "prediction", predictions]]