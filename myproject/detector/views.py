import os
from django.conf import settings
from django.core.files.storage import default_storage
from django.shortcuts import render

from detector.face_detector import Main


def upload_image(request):
    if request.method == 'POST' and request.FILES.get('image'):
        image = request.FILES['image']

        tmp_dir = os.path.join(settings.MEDIA_ROOT, 'tmp')
        os.makedirs(tmp_dir, exist_ok=True)
        temp_path = os.path.join(tmp_dir, image.name)

        path = default_storage.save(temp_path, image)
        absolute_path = os.path.join(settings.MEDIA_ROOT, path)

        if not os.path.exists(absolute_path) or os.stat(absolute_path).st_size == 0:
            return render(request, 'detector/result.html', {
                'classification_result': 'File upload failed or file is empty',
                'landmarks_result': 'N/A'
            })

        shape_result = Main(absolute_path).run_detection_stillshot()

        split_result = shape_result.split('\n')

        classification_result = split_result[0] if len(split_result) > 0 else "No classification"
        landmarks_result = split_result[1] if len(split_result) > 1 else "No landmarks"

        hairstyle_suggestions = split_result[2:] if len(split_result) > 2 else []

        return render(request, 'detector/result.html', {
            'classification_result': classification_result,
            'landmarks_result': landmarks_result,
            'hairstyle_suggestions': hairstyle_suggestions
        })
    
    return render(request, 'detector/upload.html')

